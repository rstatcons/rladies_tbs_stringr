# Strings manipulation with R-package stringr

## Announcement 

In this session we will go through basics of string manipulation in R. We will concentrate on stringr package, which provides consistent, efficient and encoding-aware tools to work with character strings.

We will concentrate on stringr package, which provides consistent, efficient and encoding-aware tools to work with character strings.

As a demonstration, we will deal with babynames package, which contains US children names from 1880 to 2015. We will find the most popular first letter and length of name for each year. As supportive packages we will rely on dplyr for data manipulation and on ggplot2 for visualization.


------
DRAFT
------

Tbilisi R-Ladies group, 2017-06-08

1. Typical tasks with strings
2. Factors vs Characters in R
  * How to convert factors to integers/numerics
3. Tools in R to work with strings
  * Base R
    * is.character, nchar, paste, paste0
    * letters, LETTERS
    * tolower, toupper, cartr
    * rep, rep.int
  * stringr (now based on stringi)
4. Regular expressions

## Sources

Gaston Sanchez. [Handling and Processing Strings in R](
http://gastonsanchez.com/Handling_and_Processing_Strings_in_R.pdf)

## Brain storming

* Convert age ranges "10-15", "16-35" into numbers ("10-15" becomes 12.5)
* Gender from middle name (in Russian)
* Convert string with first and last name to initials
* Most popular length of name 
* Most popular first character in name


